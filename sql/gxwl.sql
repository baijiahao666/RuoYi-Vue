-- ----------------------------
-- 20、考试过程表
-- ----------------------------
drop table if exists gxwl_exam_course;
create table gxwl_exam_course
(
    course_id            varchar(255) not null primary key comment '过程id',
    grade_id             varchar(255) not null comment '成绩id',
    interface_code       varchar(255) comment 'interface_code',
    booking_id           varchar(255) comment 'booking_id',
    score_details_id     varchar(255) comment 'score_details_id',
    exam_date            datetime COMMENT '121',
    exam_times           int COMMENT '1412',
    write_time           datetime COMMENT '1123',
    photo_id             varchar(255) comment 'test1',
    item_code            varchar(255) comment 'test2',
    route_no             varchar(255) comment 'test3',
    dedu_code            varchar(255) comment 'test4',
    dedu_score           int COMMENT '15',
    manual_judge         int COMMENT '16',
    speed                int COMMENT '17',
    exam_score           int COMMENT '18',
    carno                varchar(255) comment 'test9',
    license_no           varchar(255) comment 'test11',
    device_seqno         varchar(255) comment 'test12',
    replace_flag         int COMMENT '13',
    sign_int_time_code   varchar(300) comment 'test14',
    operation_type       varchar(255) comment 'test15',
    exam_room_state      varchar(255) comment 'test16',
    exception_type       varchar(255) comment 'test17',
    pause_reason         varchar(600) comment 'test18',
    apply_content        varchar(600) comment 'test19',
    misjudged_escription varchar(600) comment 'test20',
    upload_flag          int COMMENT '21',
    checkstr_value       text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '22',
    creation_time        datetime COMMENT '23',
    creator_id           varchar(255) comment 'test24',
    last_update_time     datetime COMMENT '25',
    total_mileage        int COMMENT '26',
    raw_time             datetime COMMENT '27',
    by_state             int COMMENT '28',
    is_ykflag            int COMMENT '29',
    upload_flag_two      int COMMENT '30',
    create_by            varchar(64) default '' comment '创建者',
    create_time          datetime comment '创建时间',
    update_by            varchar(64) default '' comment '更新者',
    update_time          datetime comment '更新时间'
) engine = innodb
  auto_increment = 100 comment = '考试过程表';

-- ----------------------------
-- 21、考试成绩表
-- ----------------------------
drop table if exists gxwl_exam_result;
create table gxwl_exam_result
(
    grade_id    varchar(255) not null primary key comment '成绩id',
    sub_id      varchar(255) comment '预约id',
    score       varchar(255) comment '总分',
    result      varchar(255) comment '结果',
    create_by   varchar(64) default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64) default '' comment '更新者',
    update_time datetime comment '更新时间'
) engine = innodb
  auto_increment = 100 comment = '考试成绩表';

-- ----------------------------
-- 22、预约表/签到表/组表
-- ----------------------------
drop table if exists gxwl_exam_subscribe;
create table gxwl_exam_subscribe
(
    sub_id               varchar(255) not null primary key comment '预约id',
    name                 varchar(255) comment '姓名',
    phone                varchar(11) comment '电话',
    sex                  char(1)     default '0' comment '性别（0男 1女 2未知）',
    ident_card           varchar(255) comment '身份证号',
    sub_time             datetime comment '预约考试时间',
    is_sign              char(1) comment '是否签到（1是）',
    sign_time            datetime comment '签到时间',
    group_name           varchar(255) comment '组名',
    group_manage_user_id bigint(20) comment '教练ID',
    group_manage_name    varchar(255) comment '教练姓名',
    create_by            varchar(64) default '' comment '创建者',
    create_time          datetime comment '创建时间',
    update_by            varchar(64) default '' comment '更新者',
    update_time          datetime comment '更新时间'
) engine = innodb
  auto_increment = 100 comment = '预约表';