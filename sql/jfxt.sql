-- ----------------------------
-- 1、学生信息表
-- ----------------------------
drop table if exists jfxt_student;
create table jfxt_student
(
    stu_id      varchar(255) not null primary key comment '学生id',
    stu_name    varchar(255) comment '学生姓名',
    school_name varchar(50) comment '学校名称',
    phone       varchar(11) comment '电话',
    sex         char(1)     default '0' comment '性别（0男 1女 2未知）',
    stu_status  char(1)     default '0' comment '在校状态（0在校 1离校）',
    charge_category char(1) comment '收费分类',
    ident_card  varchar(255) comment '身份证号',
    create_by   varchar(64) default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64) default '' comment '更新者',
    update_time datetime comment '更新时间'
) engine = innodb
  auto_increment = 100 comment = '学生信息表';

-- ----------------------------
-- 1、员工信息表
-- ----------------------------
drop table if exists jfxt_employee;
create table jfxt_employee
(
    emp_id          varchar(255) not null primary key comment '员工id',
    emp_name        varchar(255) comment '员工姓名',
    phone           varchar(11) comment '电话',
    ident_card      varchar(255) comment '身份证号',
    sex             char(1)     default '0' comment '性别（0男 1女 2未知）',
    job_status      char(1)     default '0' comment '在职状态（0在职 1离职）',
    emp_category    char(1) comment '员工分类',
    charge_category char(1) comment '收费分类',
    create_by       varchar(64) default '' comment '创建者',
    create_time     datetime comment '创建时间',
    update_by       varchar(64) default '' comment '更新者',
    update_time     datetime comment '更新时间'
) engine = innodb
  auto_increment = 100 comment = '员工信息表';