package com.ruoyi.jfxt.service.impl;

import java.util.List;

import cn.hutool.core.util.IdUtil;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.jfxt.mapper.JfxtEmployeeMapper;
import com.ruoyi.jfxt.domain.JfxtEmployee;
import com.ruoyi.jfxt.service.IJfxtEmployeeService;

/**
 * 员工信息Service业务层处理
 * 
 * @author baijhc
 * @date 2024-07-09
 */
@Service
public class JfxtEmployeeServiceImpl implements IJfxtEmployeeService 
{
    @Autowired
    private JfxtEmployeeMapper jfxtEmployeeMapper;

    /**
     * 查询员工信息
     * 
     * @param empId 员工信息主键
     * @return 员工信息
     */
    @Override
    public JfxtEmployee selectJfxtEmployeeByEmpId(String empId)
    {
        return jfxtEmployeeMapper.selectJfxtEmployeeByEmpId(empId);
    }

    /**
     * 查询员工信息列表
     * 
     * @param jfxtEmployee 员工信息
     * @return 员工信息
     */
    @Override
    public List<JfxtEmployee> selectJfxtEmployeeList(JfxtEmployee jfxtEmployee)
    {
        return jfxtEmployeeMapper.selectJfxtEmployeeList(jfxtEmployee);
    }

    /**
     * 新增员工信息
     * 
     * @param jfxtEmployee 员工信息
     * @return 结果
     */
    @Override
    public int insertJfxtEmployee(JfxtEmployee jfxtEmployee)
    {
        jfxtEmployee.setCreateTime(DateUtils.getNowDate());
        jfxtEmployee.setEmpId(IdUtil.getSnowflakeNextIdStr());
        return jfxtEmployeeMapper.insertJfxtEmployee(jfxtEmployee);
    }

    /**
     * 修改员工信息
     * 
     * @param jfxtEmployee 员工信息
     * @return 结果
     */
    @Override
    public int updateJfxtEmployee(JfxtEmployee jfxtEmployee)
    {
        jfxtEmployee.setUpdateTime(DateUtils.getNowDate());
        return jfxtEmployeeMapper.updateJfxtEmployee(jfxtEmployee);
    }

    /**
     * 批量删除员工信息
     * 
     * @param empIds 需要删除的员工信息主键
     * @return 结果
     */
    @Override
    public int deleteJfxtEmployeeByEmpIds(String[] empIds)
    {
        return jfxtEmployeeMapper.deleteJfxtEmployeeByEmpIds(empIds);
    }

    /**
     * 删除员工信息信息
     * 
     * @param empId 员工信息主键
     * @return 结果
     */
    @Override
    public int deleteJfxtEmployeeByEmpId(String empId)
    {
        return jfxtEmployeeMapper.deleteJfxtEmployeeByEmpId(empId);
    }
}
