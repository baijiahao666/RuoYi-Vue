package com.ruoyi.jfxt.service;

import java.util.List;
import com.ruoyi.jfxt.domain.JfxtStudent;

/**
 * 学生信息Service接口
 * 
 * @author baijhc
 * @date 2024-07-09
 */
public interface IJfxtStudentService 
{
    /**
     * 查询学生信息
     * 
     * @param stuId 学生信息主键
     * @return 学生信息
     */
    public JfxtStudent selectJfxtStudentByStuId(String stuId);

    /**
     * 查询学生信息列表
     * 
     * @param jfxtStudent 学生信息
     * @return 学生信息集合
     */
    public List<JfxtStudent> selectJfxtStudentList(JfxtStudent jfxtStudent);

    /**
     * 新增学生信息
     * 
     * @param jfxtStudent 学生信息
     * @return 结果
     */
    public int insertJfxtStudent(JfxtStudent jfxtStudent);

    /**
     * 修改学生信息
     * 
     * @param jfxtStudent 学生信息
     * @return 结果
     */
    public int updateJfxtStudent(JfxtStudent jfxtStudent);

    /**
     * 批量删除学生信息
     * 
     * @param stuIds 需要删除的学生信息主键集合
     * @return 结果
     */
    public int deleteJfxtStudentByStuIds(String[] stuIds);

    /**
     * 删除学生信息信息
     * 
     * @param stuId 学生信息主键
     * @return 结果
     */
    public int deleteJfxtStudentByStuId(String stuId);
}
