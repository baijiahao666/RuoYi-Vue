package com.ruoyi.jfxt.service;

import java.util.List;
import com.ruoyi.jfxt.domain.JfxtEmployee;

/**
 * 员工信息Service接口
 * 
 * @author baijhc
 * @date 2024-07-09
 */
public interface IJfxtEmployeeService 
{
    /**
     * 查询员工信息
     * 
     * @param empId 员工信息主键
     * @return 员工信息
     */
    public JfxtEmployee selectJfxtEmployeeByEmpId(String empId);

    /**
     * 查询员工信息列表
     * 
     * @param jfxtEmployee 员工信息
     * @return 员工信息集合
     */
    public List<JfxtEmployee> selectJfxtEmployeeList(JfxtEmployee jfxtEmployee);

    /**
     * 新增员工信息
     * 
     * @param jfxtEmployee 员工信息
     * @return 结果
     */
    public int insertJfxtEmployee(JfxtEmployee jfxtEmployee);

    /**
     * 修改员工信息
     * 
     * @param jfxtEmployee 员工信息
     * @return 结果
     */
    public int updateJfxtEmployee(JfxtEmployee jfxtEmployee);

    /**
     * 批量删除员工信息
     * 
     * @param empIds 需要删除的员工信息主键集合
     * @return 结果
     */
    public int deleteJfxtEmployeeByEmpIds(String[] empIds);

    /**
     * 删除员工信息信息
     * 
     * @param empId 员工信息主键
     * @return 结果
     */
    public int deleteJfxtEmployeeByEmpId(String empId);
}
