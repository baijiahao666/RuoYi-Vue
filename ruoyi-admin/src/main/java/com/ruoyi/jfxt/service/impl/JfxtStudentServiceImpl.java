package com.ruoyi.jfxt.service.impl;

import java.util.List;

import cn.hutool.core.util.IdUtil;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.jfxt.mapper.JfxtStudentMapper;
import com.ruoyi.jfxt.domain.JfxtStudent;
import com.ruoyi.jfxt.service.IJfxtStudentService;

/**
 * 学生信息Service业务层处理
 * 
 * @author baijhc
 * @date 2024-07-09
 */
@Service
public class JfxtStudentServiceImpl implements IJfxtStudentService 
{
    @Autowired
    private JfxtStudentMapper jfxtStudentMapper;

    /**
     * 查询学生信息
     * 
     * @param stuId 学生信息主键
     * @return 学生信息
     */
    @Override
    public JfxtStudent selectJfxtStudentByStuId(String stuId)
    {
        return jfxtStudentMapper.selectJfxtStudentByStuId(stuId);
    }

    /**
     * 查询学生信息列表
     * 
     * @param jfxtStudent 学生信息
     * @return 学生信息
     */
    @Override
    public List<JfxtStudent> selectJfxtStudentList(JfxtStudent jfxtStudent)
    {
        return jfxtStudentMapper.selectJfxtStudentList(jfxtStudent);
    }

    /**
     * 新增学生信息
     * 
     * @param jfxtStudent 学生信息
     * @return 结果
     */
    @Override
    public int insertJfxtStudent(JfxtStudent jfxtStudent)
    {
        jfxtStudent.setCreateTime(DateUtils.getNowDate());
        jfxtStudent.setStuId(IdUtil.getSnowflakeNextIdStr());
        return jfxtStudentMapper.insertJfxtStudent(jfxtStudent);
    }

    /**
     * 修改学生信息
     * 
     * @param jfxtStudent 学生信息
     * @return 结果
     */
    @Override
    public int updateJfxtStudent(JfxtStudent jfxtStudent)
    {
        jfxtStudent.setUpdateTime(DateUtils.getNowDate());
        return jfxtStudentMapper.updateJfxtStudent(jfxtStudent);
    }

    /**
     * 批量删除学生信息
     * 
     * @param stuIds 需要删除的学生信息主键
     * @return 结果
     */
    @Override
    public int deleteJfxtStudentByStuIds(String[] stuIds)
    {
        return jfxtStudentMapper.deleteJfxtStudentByStuIds(stuIds);
    }

    /**
     * 删除学生信息信息
     * 
     * @param stuId 学生信息主键
     * @return 结果
     */
    @Override
    public int deleteJfxtStudentByStuId(String stuId)
    {
        return jfxtStudentMapper.deleteJfxtStudentByStuId(stuId);
    }
}
