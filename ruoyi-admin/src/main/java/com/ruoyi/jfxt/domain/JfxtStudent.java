package com.ruoyi.jfxt.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 学生信息对象 jfxt_student
 * 
 * @author baijhc
 * @date 2024-07-09
 */
public class JfxtStudent extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 学生id */
    private String stuId;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String stuName;

    /** 学校名称 */
    @Excel(name = "学校名称")
    private String schoolName;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 性别（0男 1女 2未知） */
    @Excel(name = "性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 在校状态（0在校 1离校） */
    @Excel(name = "在校状态", readConverterExp = "0=在校,1=离校")
    private String stuStatus;

    /** 收费分类 */
    @Excel(name = "收费分类")
    private String chargeCategory;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String identCard;

    public void setStuId(String stuId) 
    {
        this.stuId = stuId;
    }

    public String getStuId() 
    {
        return stuId;
    }
    public void setStuName(String stuName) 
    {
        this.stuName = stuName;
    }

    public String getStuName() 
    {
        return stuName;
    }
    public void setSchoolName(String schoolName) 
    {
        this.schoolName = schoolName;
    }

    public String getSchoolName() 
    {
        return schoolName;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setSex(String sex) 
    {
        this.sex = sex;
    }

    public String getSex() 
    {
        return sex;
    }
    public void setStuStatus(String stuStatus) 
    {
        this.stuStatus = stuStatus;
    }

    public String getStuStatus() 
    {
        return stuStatus;
    }
    public void setChargeCategory(String chargeCategory) 
    {
        this.chargeCategory = chargeCategory;
    }

    public String getChargeCategory() 
    {
        return chargeCategory;
    }
    public void setIdentCard(String identCard) 
    {
        this.identCard = identCard;
    }

    public String getIdentCard() 
    {
        return identCard;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("stuId", getStuId())
            .append("stuName", getStuName())
            .append("schoolName", getSchoolName())
            .append("phone", getPhone())
            .append("sex", getSex())
            .append("stuStatus", getStuStatus())
            .append("chargeCategory", getChargeCategory())
            .append("identCard", getIdentCard())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
