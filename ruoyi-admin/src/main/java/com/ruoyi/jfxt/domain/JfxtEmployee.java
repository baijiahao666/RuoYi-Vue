package com.ruoyi.jfxt.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 员工信息对象 jfxt_employee
 * 
 * @author baijhc
 * @date 2024-07-09
 */
public class JfxtEmployee extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 员工id */
    private String empId;

    /** 员工姓名 */
    @Excel(name = "员工姓名")
    private String empName;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String identCard;

    /** 性别（0男 1女 2未知） */
    @Excel(name = "性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 在职状态（0在职 1离职） */
    @Excel(name = "在职状态", readConverterExp = "0=在职,1=离职")
    private String jobStatus;

    /** 员工分类 */
    @Excel(name = "员工分类")
    private String empCategory;

    /** 收费分类 */
    @Excel(name = "收费分类")
    private String chargeCategory;

    public void setEmpId(String empId) 
    {
        this.empId = empId;
    }

    public String getEmpId() 
    {
        return empId;
    }
    public void setEmpName(String empName) 
    {
        this.empName = empName;
    }

    public String getEmpName() 
    {
        return empName;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setIdentCard(String identCard) 
    {
        this.identCard = identCard;
    }

    public String getIdentCard() 
    {
        return identCard;
    }
    public void setSex(String sex) 
    {
        this.sex = sex;
    }

    public String getSex() 
    {
        return sex;
    }
    public void setJobStatus(String jobStatus) 
    {
        this.jobStatus = jobStatus;
    }

    public String getJobStatus() 
    {
        return jobStatus;
    }
    public void setEmpCategory(String empCategory) 
    {
        this.empCategory = empCategory;
    }

    public String getEmpCategory() 
    {
        return empCategory;
    }
    public void setChargeCategory(String chargeCategory) 
    {
        this.chargeCategory = chargeCategory;
    }

    public String getChargeCategory() 
    {
        return chargeCategory;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("empId", getEmpId())
            .append("empName", getEmpName())
            .append("phone", getPhone())
            .append("identCard", getIdentCard())
            .append("sex", getSex())
            .append("jobStatus", getJobStatus())
            .append("empCategory", getEmpCategory())
            .append("chargeCategory", getChargeCategory())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
