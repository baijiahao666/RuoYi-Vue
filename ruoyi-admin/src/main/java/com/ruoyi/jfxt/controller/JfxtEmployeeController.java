package com.ruoyi.jfxt.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.jfxt.domain.JfxtEmployee;
import com.ruoyi.jfxt.service.IJfxtEmployeeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 员工信息Controller
 * 
 * @author baijhc
 * @date 2024-07-09
 */
@RestController
@RequestMapping("/jfxt/employee")
public class JfxtEmployeeController extends BaseController
{
    @Autowired
    private IJfxtEmployeeService jfxtEmployeeService;

    /**
     * 查询员工信息列表
     */
    @PreAuthorize("@ss.hasPermi('jfxt:employee:list')")
    @GetMapping("/list")
    public TableDataInfo list(JfxtEmployee jfxtEmployee)
    {
        startPage();
        List<JfxtEmployee> list = jfxtEmployeeService.selectJfxtEmployeeList(jfxtEmployee);
        return getDataTable(list);
    }

    /**
     * 导出员工信息列表
     */
    @PreAuthorize("@ss.hasPermi('jfxt:employee:export')")
    @Log(title = "员工信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, JfxtEmployee jfxtEmployee)
    {
        List<JfxtEmployee> list = jfxtEmployeeService.selectJfxtEmployeeList(jfxtEmployee);
        ExcelUtil<JfxtEmployee> util = new ExcelUtil<JfxtEmployee>(JfxtEmployee.class);
        util.exportExcel(response, list, "员工信息数据");
    }

    /**
     * 获取员工信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('jfxt:employee:query')")
    @GetMapping(value = "/{empId}")
    public AjaxResult getInfo(@PathVariable("empId") String empId)
    {
        return success(jfxtEmployeeService.selectJfxtEmployeeByEmpId(empId));
    }

    /**
     * 新增员工信息
     */
    @PreAuthorize("@ss.hasPermi('jfxt:employee:add')")
    @Log(title = "员工信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody JfxtEmployee jfxtEmployee)
    {
        return toAjax(jfxtEmployeeService.insertJfxtEmployee(jfxtEmployee));
    }

    /**
     * 修改员工信息
     */
    @PreAuthorize("@ss.hasPermi('jfxt:employee:edit')")
    @Log(title = "员工信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody JfxtEmployee jfxtEmployee)
    {
        return toAjax(jfxtEmployeeService.updateJfxtEmployee(jfxtEmployee));
    }

    /**
     * 删除员工信息
     */
    @PreAuthorize("@ss.hasPermi('jfxt:employee:remove')")
    @Log(title = "员工信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{empIds}")
    public AjaxResult remove(@PathVariable String[] empIds)
    {
        return toAjax(jfxtEmployeeService.deleteJfxtEmployeeByEmpIds(empIds));
    }
}
