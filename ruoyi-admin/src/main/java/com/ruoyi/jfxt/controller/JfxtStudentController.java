package com.ruoyi.jfxt.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.jfxt.domain.JfxtStudent;
import com.ruoyi.jfxt.service.IJfxtStudentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 学生信息Controller
 * 
 * @author baijhc
 * @date 2024-07-09
 */
@RestController
@RequestMapping("/jfxt/student")
public class JfxtStudentController extends BaseController
{
    @Autowired
    private IJfxtStudentService jfxtStudentService;

    /**
     * 查询学生信息列表
     */
    @PreAuthorize("@ss.hasPermi('jfxt:student:list')")
    @GetMapping("/list")
    public TableDataInfo list(JfxtStudent jfxtStudent)
    {
        startPage();
        List<JfxtStudent> list = jfxtStudentService.selectJfxtStudentList(jfxtStudent);
        return getDataTable(list);
    }

    /**
     * 导出学生信息列表
     */
    @PreAuthorize("@ss.hasPermi('jfxt:student:export')")
    @Log(title = "学生信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, JfxtStudent jfxtStudent)
    {
        List<JfxtStudent> list = jfxtStudentService.selectJfxtStudentList(jfxtStudent);
        ExcelUtil<JfxtStudent> util = new ExcelUtil<JfxtStudent>(JfxtStudent.class);
        util.exportExcel(response, list, "学生信息数据");
    }

    /**
     * 获取学生信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('jfxt:student:query')")
    @GetMapping(value = "/{stuId}")
    public AjaxResult getInfo(@PathVariable("stuId") String stuId)
    {
        return success(jfxtStudentService.selectJfxtStudentByStuId(stuId));
    }

    /**
     * 新增学生信息
     */
    @PreAuthorize("@ss.hasPermi('jfxt:student:add')")
    @Log(title = "学生信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody JfxtStudent jfxtStudent)
    {
        return toAjax(jfxtStudentService.insertJfxtStudent(jfxtStudent));
    }

    /**
     * 修改学生信息
     */
    @PreAuthorize("@ss.hasPermi('jfxt:student:edit')")
    @Log(title = "学生信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody JfxtStudent jfxtStudent)
    {
        return toAjax(jfxtStudentService.updateJfxtStudent(jfxtStudent));
    }

    /**
     * 删除学生信息
     */
    @PreAuthorize("@ss.hasPermi('jfxt:student:remove')")
    @Log(title = "学生信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{stuIds}")
    public AjaxResult remove(@PathVariable String[] stuIds)
    {
        return toAjax(jfxtStudentService.deleteJfxtStudentByStuIds(stuIds));
    }
}
