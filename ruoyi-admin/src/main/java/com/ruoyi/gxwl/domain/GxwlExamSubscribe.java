package com.ruoyi.gxwl.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 预约对象 gxwl_exam_subscribe
 * 
 * @author ruoyi
 * @date 2024-06-30
 */
public class GxwlExamSubscribe extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 预约id */
    private String subId;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 性别（0男 1女 2未知） */
    @Excel(name = "性别", readConverterExp = "0=男,1=女,2=未知")
    private String sex;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String identCard;

    /** 预约考试时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "预约考试时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date subTime;

    /** 是否签到（1是） */
    @Excel(name = "是否签到", readConverterExp = "1=是")
    private String isSign;

    /** 签到时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "签到时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date signTime;

    /** 组名 */
    @Excel(name = "组名")
    private String groupName;

    /** 教练ID */
    @Excel(name = "教练ID")
    private Long groupManageUserId;

    /** 教练姓名 */
    @Excel(name = "教练姓名")
    private String groupManageName;

    public void setSubId(String subId) 
    {
        this.subId = subId;
    }

    public String getSubId() 
    {
        return subId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setSex(String sex) 
    {
        this.sex = sex;
    }

    public String getSex() 
    {
        return sex;
    }
    public void setIdentCard(String identCard) 
    {
        this.identCard = identCard;
    }

    public String getIdentCard() 
    {
        return identCard;
    }
    public void setSubTime(Date subTime) 
    {
        this.subTime = subTime;
    }

    public Date getSubTime() 
    {
        return subTime;
    }
    public void setIsSign(String isSign) 
    {
        this.isSign = isSign;
    }

    public String getIsSign() 
    {
        return isSign;
    }
    public void setSignTime(Date signTime) 
    {
        this.signTime = signTime;
    }

    public Date getSignTime() 
    {
        return signTime;
    }
    public void setGroupName(String groupName) 
    {
        this.groupName = groupName;
    }

    public String getGroupName() 
    {
        return groupName;
    }
    public void setGroupManageUserId(Long groupManageUserId) 
    {
        this.groupManageUserId = groupManageUserId;
    }

    public Long getGroupManageUserId() 
    {
        return groupManageUserId;
    }
    public void setGroupManageName(String groupManageName) 
    {
        this.groupManageName = groupManageName;
    }

    public String getGroupManageName() 
    {
        return groupManageName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("subId", getSubId())
            .append("name", getName())
            .append("phone", getPhone())
            .append("sex", getSex())
            .append("identCard", getIdentCard())
            .append("subTime", getSubTime())
            .append("isSign", getIsSign())
            .append("signTime", getSignTime())
            .append("groupName", getGroupName())
            .append("groupManageUserId", getGroupManageUserId())
            .append("groupManageName", getGroupManageName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
