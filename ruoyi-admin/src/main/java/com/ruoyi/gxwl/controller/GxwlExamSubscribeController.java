package com.ruoyi.gxwl.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.gxwl.domain.GxwlExamSubscribe;
import com.ruoyi.gxwl.service.IGxwlExamSubscribeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 预约Controller
 * 
 * @author ruoyi
 * @date 2024-06-30
 */
@RestController
@RequestMapping("/gxwl/subscribe")
public class GxwlExamSubscribeController extends BaseController
{
    @Autowired
    private IGxwlExamSubscribeService gxwlExamSubscribeService;

    /**
     * 查询预约列表
     */
    @PreAuthorize("@ss.hasPermi('gxwl:subscribe:list')")
    @GetMapping("/list")
    public TableDataInfo list(GxwlExamSubscribe gxwlExamSubscribe)
    {
        startPage();
        List<GxwlExamSubscribe> list = gxwlExamSubscribeService.selectGxwlExamSubscribeList(gxwlExamSubscribe);
        return getDataTable(list);
    }

    /**
     * 导出预约列表
     */
    @PreAuthorize("@ss.hasPermi('gxwl:subscribe:export')")
    @Log(title = "预约", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, GxwlExamSubscribe gxwlExamSubscribe)
    {
        List<GxwlExamSubscribe> list = gxwlExamSubscribeService.selectGxwlExamSubscribeList(gxwlExamSubscribe);
        ExcelUtil<GxwlExamSubscribe> util = new ExcelUtil<GxwlExamSubscribe>(GxwlExamSubscribe.class);
        util.exportExcel(response, list, "预约数据");
    }

    /**
     * 获取预约详细信息
     */
    @PreAuthorize("@ss.hasPermi('gxwl:subscribe:query')")
    @GetMapping(value = "/{subId}")
    public AjaxResult getInfo(@PathVariable("subId") String subId)
    {
        return success(gxwlExamSubscribeService.selectGxwlExamSubscribeBySubId(subId));
    }

    /**
     * 新增预约
     */
    @PreAuthorize("@ss.hasPermi('gxwl:subscribe:add')")
    @Log(title = "预约", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GxwlExamSubscribe gxwlExamSubscribe)
    {
        return toAjax(gxwlExamSubscribeService.insertGxwlExamSubscribe(gxwlExamSubscribe));
    }

    /**
     * 修改预约
     */
    @PreAuthorize("@ss.hasPermi('gxwl:subscribe:edit')")
    @Log(title = "预约", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GxwlExamSubscribe gxwlExamSubscribe)
    {
        return toAjax(gxwlExamSubscribeService.updateGxwlExamSubscribe(gxwlExamSubscribe));
    }

    /**
     * 删除预约
     */
    @PreAuthorize("@ss.hasPermi('gxwl:subscribe:remove')")
    @Log(title = "预约", businessType = BusinessType.DELETE)
	@DeleteMapping("/{subIds}")
    public AjaxResult remove(@PathVariable String[] subIds)
    {
        return toAjax(gxwlExamSubscribeService.deleteGxwlExamSubscribeBySubIds(subIds));
    }
}
