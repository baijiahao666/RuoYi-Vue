package com.ruoyi.gxwl.service.impl;

import java.util.List;

import cn.hutool.core.util.IdUtil;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.gxwl.mapper.GxwlExamSubscribeMapper;
import com.ruoyi.gxwl.domain.GxwlExamSubscribe;
import com.ruoyi.gxwl.service.IGxwlExamSubscribeService;

/**
 * 预约Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-06-30
 */
@Service
public class GxwlExamSubscribeServiceImpl implements IGxwlExamSubscribeService 
{
    @Autowired
    private GxwlExamSubscribeMapper gxwlExamSubscribeMapper;

    /**
     * 查询预约
     * 
     * @param subId 预约主键
     * @return 预约
     */
    @Override
    public GxwlExamSubscribe selectGxwlExamSubscribeBySubId(String subId)
    {
        return gxwlExamSubscribeMapper.selectGxwlExamSubscribeBySubId(subId);
    }

    /**
     * 查询预约列表
     * 
     * @param gxwlExamSubscribe 预约
     * @return 预约
     */
    @Override
    public List<GxwlExamSubscribe> selectGxwlExamSubscribeList(GxwlExamSubscribe gxwlExamSubscribe)
    {
        return gxwlExamSubscribeMapper.selectGxwlExamSubscribeList(gxwlExamSubscribe);
    }

    /**
     * 新增预约
     * 
     * @param gxwlExamSubscribe 预约
     * @return 结果
     */
    @Override
    public int insertGxwlExamSubscribe(GxwlExamSubscribe gxwlExamSubscribe)
    {
        gxwlExamSubscribe.setSubId(IdUtil.getSnowflakeNextIdStr());
        gxwlExamSubscribe.setCreateTime(DateUtils.getNowDate());
        return gxwlExamSubscribeMapper.insertGxwlExamSubscribe(gxwlExamSubscribe);
    }

    /**
     * 修改预约
     * 
     * @param gxwlExamSubscribe 预约
     * @return 结果
     */
    @Override
    public int updateGxwlExamSubscribe(GxwlExamSubscribe gxwlExamSubscribe)
    {
        gxwlExamSubscribe.setUpdateTime(DateUtils.getNowDate());
        return gxwlExamSubscribeMapper.updateGxwlExamSubscribe(gxwlExamSubscribe);
    }

    /**
     * 批量删除预约
     * 
     * @param subIds 需要删除的预约主键
     * @return 结果
     */
    @Override
    public int deleteGxwlExamSubscribeBySubIds(String[] subIds)
    {
        return gxwlExamSubscribeMapper.deleteGxwlExamSubscribeBySubIds(subIds);
    }

    /**
     * 删除预约信息
     * 
     * @param subId 预约主键
     * @return 结果
     */
    @Override
    public int deleteGxwlExamSubscribeBySubId(String subId)
    {
        return gxwlExamSubscribeMapper.deleteGxwlExamSubscribeBySubId(subId);
    }
}
