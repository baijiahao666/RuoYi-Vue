package com.ruoyi.gxwl.service;

import java.util.List;
import com.ruoyi.gxwl.domain.GxwlExamSubscribe;

/**
 * 预约Service接口
 * 
 * @author ruoyi
 * @date 2024-06-30
 */
public interface IGxwlExamSubscribeService 
{
    /**
     * 查询预约
     * 
     * @param subId 预约主键
     * @return 预约
     */
    public GxwlExamSubscribe selectGxwlExamSubscribeBySubId(String subId);

    /**
     * 查询预约列表
     * 
     * @param gxwlExamSubscribe 预约
     * @return 预约集合
     */
    public List<GxwlExamSubscribe> selectGxwlExamSubscribeList(GxwlExamSubscribe gxwlExamSubscribe);

    /**
     * 新增预约
     * 
     * @param gxwlExamSubscribe 预约
     * @return 结果
     */
    public int insertGxwlExamSubscribe(GxwlExamSubscribe gxwlExamSubscribe);

    /**
     * 修改预约
     * 
     * @param gxwlExamSubscribe 预约
     * @return 结果
     */
    public int updateGxwlExamSubscribe(GxwlExamSubscribe gxwlExamSubscribe);

    /**
     * 批量删除预约
     * 
     * @param subIds 需要删除的预约主键集合
     * @return 结果
     */
    public int deleteGxwlExamSubscribeBySubIds(String[] subIds);

    /**
     * 删除预约信息
     * 
     * @param subId 预约主键
     * @return 结果
     */
    public int deleteGxwlExamSubscribeBySubId(String subId);
}
