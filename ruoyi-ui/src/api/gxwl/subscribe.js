import request from '@/utils/request'

// 查询预约列表
export function listSubscribe(query) {
  return request({
    url: '/gxwl/subscribe/list',
    method: 'get',
    params: query
  })
}

// 查询预约详细
export function getSubscribe(subId) {
  return request({
    url: '/gxwl/subscribe/' + subId,
    method: 'get'
  })
}

// 新增预约
export function addSubscribe(data) {
  return request({
    url: '/gxwl/subscribe',
    method: 'post',
    data: data
  })
}

// 修改预约
export function updateSubscribe(data) {
  return request({
    url: '/gxwl/subscribe',
    method: 'put',
    data: data
  })
}

// 删除预约
export function delSubscribe(subId) {
  return request({
    url: '/gxwl/subscribe/' + subId,
    method: 'delete'
  })
}
